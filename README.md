# evelibs

Base libraries to use as foundation when building 3rd-party Eve Online applications.
* https://crest-tq.eveonline.com - Live CREST API
* https://api.eveonline.com/ - Live XML API
* https://image.eveonline.com/ - Live image server
* https://api-sisi.testeveonline.com/ - Test CREST API
* https://api.testeveonline.com/ - Test XML API

## Documentation

* **Technology Lab at EveO Forums** [https://forums.eveonline.com/default.aspx?g=topics&f=263](https://forums.eveonline.com/default.aspx?g=topics&f=263)
* **Official 3rd Party Eve Developers** [https://developers.eveonline.com/](https://developers.eveonline.com/)
* **Third Party Documentation** [http://eveonline-third-party-documentation.readthedocs.io/en/latest/](http://eveonline-third-party-documentation.readthedocs.io/en/latest/)
