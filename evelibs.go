// Copyright 2016 Valar Morghulis..  All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*
	Package evelibs implements generic eve data-lookup
	functions

	Usage:
*/
package evelibs

import (
	"net/http"
	"net/url"
	"reflect"
)

const (
	libraryVersion = "0.0.1"
	defaultBaseURL = ""
	mediaType      = "application/json"
)

// Client manages communication with various APIs
type Client struct {
	// HTTP client used to communicate with the API
	client *http.Client

	// Base URL for API requests
	BaseURL *url.URL

	// User agent for client
	UserAgent string

	// Rate contains the current rate limit for the client as determined by the most recent
	// API call
	Rate Rate
}

// RequestCompletionCallback defines the type of request callback function
type RequestCompletionCallback func(*http.Request, *http.Response)

// ListOptions specifies the optional parameters to various List methods that
// support pagination.
type ListOptions struct {
	// For paginated result sets, page of results to retrieve.
	Page int `url:"page,omitempty"`

	// For paginated result sets, the number of results to include per page.
	PerPage int `url:"per_page,omitempty"`
}

// Response is an API response.  This wraps the standard http.Response returned
type Response struct {
	*http.Response

	// Links that were returned with the response.  These are parsed from
	// request body and not the header
	Links *Links

	// Monitoring URI
	Monitor string

	Rate
}

// An ErrorResponse reports the error caused by an API request
type ErrorResponse struct {
	// HTTP response that caused this error
	Response *http.Response

	// Error message
	Message string
}

// Rate contains the rate limit for the current client
type Rate struct {
	// The number of requests per hour the client is currently limited to
	Limit int `json:"limit"`

	// The number of remaining requests the client can make this hour
	Remaining int `json:"remaining"`

	// The time at which the current rate limit will reset.
	Reset Timestamp `json:"reset"`
}

func addOptions(s string, opt interface{}) (string, error) {
	v := reflect.ValueOf(opt)

	if v.Kind() == reflect.Ptr && v.IsNil() {
		return s, nil
	}

	origURL, err := url.Parse(s)
	if err != nil {
		return s, err
	}

	origValues := origURL.Query()

	newValues, err := query.Values(opt)
	if err != nil {
		return s, err
	}

	for k, v := range newValues {
		origValues[k] = v
	}

	origURL.RawQuery = origValues.Encode()
	return origURL.String(), nil
}

// NewClient returns a new API client
func NewClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	baseURL, _ := url.Parse(defaultBaseURL)

	c := &Client(client: httpClient, BaseURL: baseURL, UserAgent: userAgent}

	return c
}

// ClientOpt are options for New.
type ClientOpt func(*Client) error
